import ContactForm from './components/contact-form';
import Footer from './components/footer';
import Image from "next/image";


export default function Home() {
  return (
    <div className='bg-gradient-to-r from-orange-500 to-violet-500 text-white min-h-full'>
      <div className="flex flex-col items-center space-y-12 px-8">
        <div className="flex flex-col items-center space-y-2">
          <Image
                src="/henhouse-logo-white.png"
                alt="Hen House Logo"
                className="mt-20 max-md:mt-5 mb-10 md:mb-5"
                width={200}
                height={200}
                priority
              />

          <h1 className='text-8xl font-bold'>Hen House</h1>
          <h2 className='text-3xl max-md:place-self-start'>A Nostr community for women</h2>

        </div>


        <p className='text-lg font-bold'>Sign up now for early access to this women-only Nostr community, built on Ditto.</p>
        <p className='text-lg font-bold'>Opening Monday, June 17, 2024</p>

        <ContactForm />

        <div className='max-w-prose space-y-8 pb-8'>
          <div className=''>
            <p className='text-lg font-bold '>What is Hen House?</p>
            <p className='text-lg'>Hen House is a Nostr community for women build on <a className='underline' href='https://soapbox.pub/ditto/'>Ditto</a>. Users get a henhouse.social NIP05 identity and enjoy local feed with a moderated community.</p>
          </div>
          <div className=''>
            <p className='text-lg font-bold '>Is it women-only?</p>
            <p className='text-lg'>Yes! Hen House is only open to women. This is primarily based on the honor system. Unapproved users will have their henhouse.social identity revoked and will not be able to participate in the local timeline or post to the Hen House relay.</p>
          </div>
          <div className=''>
            <p className='text-lg font-bold '>What content will be moderated on Hen House?</p>
            <p className='text-lg'>Hen House will promote open dialogue and civilized debate. Misogyny, racism, harassment, spam, and porn will not be permitted. <a className='underline' href="https://soapbox.pub/blog/">Learn more</a> about moderation and building currated communities on Ditto.</p>
          </div>
          <div className=''>
            <p className='text-lg font-bold '>Is this censorship?</p>
            <p className='text-lg'>No! Nostr is an open protocol, and we have no ability to prevent anyone from saying whatever they want - nor would we want to. We do have the ability, though, to create a better environment for ourselves where we don't have to deal with harassment. If you don't like it, you don't have to use Hen House! And you can always take your keys to another client, anyway.</p>
          </div>
          <div className=''>
            <p className='text-lg font-bold '>When will Hen House be open?</p>
            <p className='text-lg'>Ditto is still under active development, but we plan to launch as soon as a Beta is available. We'll email you when it's time!</p>
          </div>
        
        </div>

      </div>

      <Footer />

    </div>
  );
}
