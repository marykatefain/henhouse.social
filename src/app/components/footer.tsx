export default function Footer() {   
    return (
        <footer className="bottom-0 left-0 z-20 mt-20 w-full p-4 bg-white border-t border-gray-200 shadow md:flex md:items-center md:justify-between md:p-6 dark:bg-gray-800 dark:border-gray-600">
            <span className="text-sm text-gray-500 sm:text-center dark:text-gray-400">
            ♡ {new Date().getFullYear()}. Please <a className='underline' href='https://gitlab.com/marykatefain/henhouse.social' target='_blank'>copy and share</a>.
            </span>
            <ul className="flex flex-wrap items-center mt-3 text-sm font-medium text-gray-500 dark:text-gray-400 sm:mt-0">
                <li>
                    <a href="https://soapbox.pub/" className="hover:underline me-4 md:me-6">Soapbox</a>
                </li>
                <li>
                    <a href="https://soapbox.pub/ditto/" className="hover:underline me-4 md:me-6">Ditto</a>
                </li>
                <li>
                    <a href="https://marykatefain.com/software/" className="hover:underline me-4 md:me-6">About M. K. Fain</a>
                </li>
            </ul>
        </footer>
    );
}
