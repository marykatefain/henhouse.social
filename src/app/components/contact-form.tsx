'use client';

import { useForm, ValidationError } from "@formspree/react";

export default function ContactForm() { 

  const [state, handleSubmit] = useForm("mleqzrkl");

  if (state.succeeded) {
    return <p>Thanks for your submission! We'll email you when Hen House is open for registration!</p>;
  }

  return (
    <form onSubmit={handleSubmit}>

        <div className="flex flex-col items-left max-w-96 space-y-8">

            <div className="flex flex-col items-left">
                <label htmlFor="npub">NPub</label>
                <input className='text-slate-700 border-0 border-slate-400 rounded-sm p-2' 
                type="text" name="npub" id="npub" placeholder="npub123...." required />
                <ValidationError prefix="Npub" field="npub" errors={state.errors} />
            </div>
            
            <div className="flex flex-col items-left">
                <label htmlFor="email-address">Email Address</label>
                <input className='text-slate-700 border-0 border-slate-400 rounded-sm p-2' 
                type="email" name="_replyto" id="email-address" placeholder="email@domain.tld" />
                <ValidationError prefix="Email" field="email" errors={state.errors} />
            </div>

            <div className="flex flex-col items-left">
                <label htmlFor="message">Why do you want to join the Nostr Hen House?</label>
                <textarea className='text-slate-700 border-0 border-slate-400 rounded-sm p-2' 
                rows={5} name="message" id="message" placeholder="Tell us briefly about your experience on Nostr, or how you heard about Hen House." required></textarea>
                <ValidationError prefix="Message" field="message" errors={state.errors} />
            </div>
        
        <input type="hidden" name="_subject" id="email-subject" value="Contact Form Submission" />

        <button 
            className='bg-gradient-to-r from-purple-600 to-purple-800 border-2 hover:bg-gradient-to-r hover:to-fuchsia-600 border-white rounded-lg h-12 w-1/2 font-bold place-self-center' 
            type="submit" disabled={state.submitting}>
            Submit
        </button>

        <ValidationError errors={state.errors} />

        </div>

    </form>
  );
}